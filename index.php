<?php
   use \Psr\Http\Message\ServerRequestInterface as Request;
   use \Psr\Http\Message\ResponseInterface as Response;

   require 'lib/config.php';
   require 'vendor/autoload.php';
   require 'lib/script.php';

   $app = new \Slim\App;

   $app->get('/','getNameAlbumDay');
   $app->get('/tracks/{id}',function($request, $response, $args){
		getTracks($args['id']);
    });

   $app->run();




 ?>
